<?php
    echo "<div class=\"container\">";

    if (isset($_POST['submit'])) {

        // Checkbox
        if(empty($_POST['sports'])) {
            echo "You didn't select any sport<br>";
        } else {
            $N = count($_POST['sports']);
            echo "You selected $N sport(s):<br>" ;
            foreach ($_POST['sports'] as $sport) {
                echo($sport . "<br>");
            }
        }

        // Color
        if (!empty($_POST['clr'])) {
            echo 'Your favorite color is: ' . $_POST['clr'] . '<br>';
        }

        // Date
        if(!empty($_POST['date1']) && !empty($_POST['date2'])) {
            echo 'Date1: ' . $_POST['date1'] . '<br>' . 'Date2: ' . $_POST['date2'] . '<br>';
        } else {
            echo 'You didn\'t enter the two dates<br>';
        }

        // Datetime-local
        if (!empty($_POST['bday'])) {
            echo 'Your birthday: ' . $_POST['bday'] . '<br>';
        }
        else {
            echo 'You didn\'t enter your birthday<br>';
        }

        // Email & Password
        if (!empty($_POST['email']) && !empty($_POST['password'])) {
            if($_POST['email'] == "admin@gmail.com" && $_POST['password'] == "admin") {
                echo 'Login Succeeded<br>';
            } else {
                echo 'Login Failed<br>';
            }
        } else {
            echo 'You didn\'t enter email or password<br>';
        }

        // File
        if (!empty($_POST['file'])) {
            $file = pathinfo($_POST['file']);
            echo $file['extension'], '<br>';
        }

        // hidden
        if (!empty($_POST['hid'])) {
            echo 'the hidden number is: ' . $_POST['hid'] . '<br>';
        }

        // Radio
        if (!empty($_POST['gender'])){
            echo 'you\'re ' . $_POST['gender']; 
        } else {
            echo 'you didn\'t pick a gender';
        }

    }

    echo '</div>'
?>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <title>Task-15</title>
    </head>

    <body>
        <div class="container my-3">
            <form method="post">
                <h5>Choose your favorite sports:</h5>
                <div class="form-check">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="sports[]" id="customCheck1"
                            value="Football">
                        <label class="custom-control-label" for="customCheck1">Football</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="sports[]" id="customCheck2"
                            value="Basketball">
                        <label class="custom-control-label" for="customCheck2">Basketball</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="sports[]" id="customCheck3"
                            value="Volleyball">
                        <label class="custom-control-label" for="customCheck3">Volleyball</label>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" name="sports[]" id="customCheck4"
                            value="Handball">
                        <label class="custom-control-label" for="customCheck4">Handball</label>
                    </div>
                </div>
                <div class="form-group my-2">
                    <h5>Choose your favorite color:</h5>
                    <input type="color" name="clr">
                </div>
                <div class="form-group">
                    Enter a date before 1980-01-01:
                    <input type="date" max="1970-12-31" name="date1" class="form-control"><br>
                    Enter a date after 2000-01-01:
                    <input type="date" min="2000-01-02" name="date2" class="form-control"><br>
                </div>
                <div class="form-group">
                    <h5>Enter your birthday:</h5>
                    <input type="datetime-local" name="bday" class="form-control">
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control">
                </div>
                <div class="form-group">
                    <h5>Upload file:</h5>
                    <input type="file" name="file">
                </div>
                <div class="form-group">
                    <input type="hidden" name="hid" value="19">
                </div>
                <div class="form-group">
                    <input type="image" name="img" src="img/img_submit.gif" alt="Submit" width="48" height="48">
                </div>
                <div class="form-group">
                    <label>Enter month:</label>
                    <input type="month" class="form-control">
                </div>
                <div class="form-group">
                    <label>Enter your age:</label>
                    <input type="number" class="form-control">
                </div>
                <h5>Gender:</h5>
                <div class="form-check my-3">
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio1" name="gender" value="Male" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio1">Male</label>
                    </div>
                    <div class="custom-control custom-radio">
                        <input type="radio" id="customRadio2" name="gender" value="Female" class="custom-control-input">
                        <label class="custom-control-label" for="customRadio2">Female</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="customRange1">Example range</label>
                    <input type="range" class="custom-range" id="customRange1">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Search</label>
                    <input type="search" class="form-control">
                </div>
                <div class="form-group">
                    Telephone: <input type="tel" name="phone" pattern="[0-9]{3}-[0-9]{2}-[0-9]{3}"
                        placeholder="123-45-678">
                </div>
                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control">
                </div>
                <div class="form-group">
                    <label>Enter time:</label>
                    <input type="time" class="form-control">
                </div>
                <div class="form-group">
                    <label>Enter URL</label>
                    <input type="url" class="form-control">
                </div>
                <div class="form-group">
                    <label>Enter week</label>
                    <input type="week" class="form-control">
                </div>
                <div class="form-group">
                    <input type="submit" name="submit" class="btn btn-info" value="Submit">
                </div>
                <div class="form-group">
                    <input type="reset" value="Reset Form" class="btn btn-danger">
                </div>
            </form>
            <a href="/" class="btn btn-light">Reset Page</a>
        </div>
        <script src="js/bootstrap.min.js"></script>
    </body>

</html>